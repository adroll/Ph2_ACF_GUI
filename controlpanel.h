#ifndef CONTROLPANEL_H
#define CONTROLPANEL_H

#include <QWidget>
#include <QString>
#include <QDir>
#include <QFile>
#include <QDebug>
#include <QDomDocument>
#include <QCheckBox>
#include <QThread>



#include "tests/testtab.h"
#include "tests/testhandler.h"
#include "hardware/hardwareparser.h"



namespace Ui {
class ControlPanel;
}

class ControlPanel : public QWidget
{
    Q_OBJECT

public:
    explicit ControlPanel(QWidget *parent = 0);
    ~ControlPanel();

private:
    Ui::ControlPanel *ui;

    QFile fTestXML;
    QDomDocument *fDomDoc;
    QDomNodeList fTestNodes;


    QList<QCheckBox*> fCheckBoxList;
    QList<TestTab*> fTestTabs;
    QThread *fTestThread;
    TestHandler *fTestHandler;

    HardwareParser *fHardwareParser;



signals:
    void status(QString cStatus);




private slots:
    void updateHardwareParser();
    void readTestXML();
    void addTests();
    void makeTestTab(QDomNode node);



    void appendLine(QString cString);

    void on_fStartButton_clicked();
    void on_fStartAllButton_clicked();


    void resizeEvent(QResizeEvent *e);

    void on_fTestButton_clicked();
};

#endif // CONTROLPANEL_H
