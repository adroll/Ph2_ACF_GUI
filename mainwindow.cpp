#include "mainwindow.h"
#include "ui_mainwindow.h"




MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    fControlPanel = new ControlPanel(ui->fControlWidget);

    connect(ui->actionQuit,SIGNAL(triggered()),this,SLOT(QuitModuleTesting()));
    connect(fControlPanel,SIGNAL(status(QString)),this,SLOT(showStatus(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::resizeEvent(QResizeEvent *e)
{
    fControlPanel->resize(ui->fControlWidget->width(),ui->fControlWidget->height());

}



void MainWindow::QuitModuleTesting()
{

    QApplication::quit();
}



void MainWindow::showStatus(QString cStatus)
{
    statusBar()->showMessage(cStatus);
}
