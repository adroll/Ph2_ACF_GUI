#-------------------------------------------------
#
# Project created by QtCreator 2017-07-10T12:36:34
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ModuleTesting
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tests/testtab.cpp \
    controlpanel.cpp \
    tests/testhandler.cpp \
    hardware/hardwareparser.cpp \
    hardware/dommodel.cpp \
    hardware/domitem.cpp \
    parser/pugixml.cpp

HEADERS  += mainwindow.h \
    controlpanel.h \
    tests/testtab.h \
    controlpanel.h \
    tests/testhandler.h \
    hardware/hardwareparser.h \
    hardware/dommodel.h \
    hardware/domitem.h \
    parser/pugixml.hpp \
    parser/pugiconfig.hpp

FORMS    += mainwindow.ui \
    tests/testtab.ui \
    controlpanel.ui \
    hardware/hardwareparser.ui

DISTFILES += \
    tests/tests.xml \
    hardware/D19CDescription.xml
