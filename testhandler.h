#ifndef TESTHANDLER_H
#define TESTHANDLER_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include <QProcess>
#include <QFile>


#include "tests/calibrate.h"
#include "tests/datatest.h"


class TestHandler : public QObject
{
    Q_OBJECT
public:
    explicit TestHandler(QObject *parent = 0);

    //Attributes
    QString fPh2Path;
    QString fXmlFile;

    std::vector<Test*> fTestVec;



private:
    QStringList fList;




signals:
    void print(QString cString);
    void finished();


public slots:

    int run_script();

    void BuildList(Test* cTest);

};

#endif // TESTHANDLER_H
