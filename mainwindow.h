#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QResizeEvent>

#include "controlpanel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();




private slots:
    void QuitModuleTesting();

    void showStatus(QString cStatus);

private:
    Ui::MainWindow *ui;

    void resizeEvent(QResizeEvent *e);



    //Tab Widgets:

    ControlPanel *fControlPanel;

};

#endif // MAINWINDOW_H
