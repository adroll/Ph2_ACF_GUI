/********************************************************************************
** Form generated from reading UI file 'controlpanel.ui'
**
** Created by: Qt User Interface Compiler version 5.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONTROLPANEL_H
#define UI_CONTROLPANEL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ControlPanel
{
public:
    QGridLayout *gridLayout_4;
    QTabWidget *ftabWidget;
    QWidget *fControlTab;
    QGridLayout *gridLayout;
    QWidget *fDisplay;
    QTextBrowser *fTextBrowser;
    QGroupBox *fTestBox;
    QGridLayout *fTestBoxLayout;
    QPushButton *fStartAllButton;
    QSpacerItem *verticalSpacer;
    QPushButton *fStartButton;
    QPushButton *fTestButton;
    QLabel *fTestControlLabel;
    QTextBrowser *fConsole;
    QLineEdit *fPh2Path;
    QLabel *fPh2_Label;
    QWidget *fHardwareTab;
    QGridLayout *gridLayout_2;
    QWidget *fHardwareWidget;

    void setupUi(QWidget *ControlPanel)
    {
        if (ControlPanel->objectName().isEmpty())
            ControlPanel->setObjectName(QStringLiteral("ControlPanel"));
        ControlPanel->resize(640, 480);
        gridLayout_4 = new QGridLayout(ControlPanel);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        ftabWidget = new QTabWidget(ControlPanel);
        ftabWidget->setObjectName(QStringLiteral("ftabWidget"));
        fControlTab = new QWidget();
        fControlTab->setObjectName(QStringLiteral("fControlTab"));
        gridLayout = new QGridLayout(fControlTab);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        fDisplay = new QWidget(fControlTab);
        fDisplay->setObjectName(QStringLiteral("fDisplay"));
        fTextBrowser = new QTextBrowser(fDisplay);
        fTextBrowser->setObjectName(QStringLiteral("fTextBrowser"));
        fTextBrowser->setGeometry(QRect(60, 10, 256, 192));

        gridLayout->addWidget(fDisplay, 2, 0, 1, 1);

        fTestBox = new QGroupBox(fControlTab);
        fTestBox->setObjectName(QStringLiteral("fTestBox"));
        fTestBoxLayout = new QGridLayout(fTestBox);
        fTestBoxLayout->setObjectName(QStringLiteral("fTestBoxLayout"));
        fStartAllButton = new QPushButton(fTestBox);
        fStartAllButton->setObjectName(QStringLiteral("fStartAllButton"));

        fTestBoxLayout->addWidget(fStartAllButton, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        fTestBoxLayout->addItem(verticalSpacer, 1, 0, 1, 1);

        fStartButton = new QPushButton(fTestBox);
        fStartButton->setObjectName(QStringLiteral("fStartButton"));

        fTestBoxLayout->addWidget(fStartButton, 3, 0, 1, 1);

        fTestButton = new QPushButton(fTestBox);
        fTestButton->setObjectName(QStringLiteral("fTestButton"));

        fTestBoxLayout->addWidget(fTestButton, 2, 0, 1, 1);


        gridLayout->addWidget(fTestBox, 1, 1, 2, 1);

        fTestControlLabel = new QLabel(fControlTab);
        fTestControlLabel->setObjectName(QStringLiteral("fTestControlLabel"));
        QFont font;
        font.setPointSize(28);
        font.setBold(true);
        font.setWeight(75);
        fTestControlLabel->setFont(font);
        fTestControlLabel->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(fTestControlLabel, 0, 0, 1, 2);

        fConsole = new QTextBrowser(fControlTab);
        fConsole->setObjectName(QStringLiteral("fConsole"));

        gridLayout->addWidget(fConsole, 1, 0, 1, 1);

        fPh2Path = new QLineEdit(fControlTab);
        fPh2Path->setObjectName(QStringLiteral("fPh2Path"));

        gridLayout->addWidget(fPh2Path, 3, 0, 1, 1);

        fPh2_Label = new QLabel(fControlTab);
        fPh2_Label->setObjectName(QStringLiteral("fPh2_Label"));

        gridLayout->addWidget(fPh2_Label, 3, 1, 1, 1);

        gridLayout->setRowStretch(0, 1);
        gridLayout->setRowStretch(1, 5);
        gridLayout->setRowStretch(2, 7);
        gridLayout->setColumnStretch(0, 5);
        gridLayout->setColumnStretch(1, 1);
        ftabWidget->addTab(fControlTab, QString());
        fHardwareTab = new QWidget();
        fHardwareTab->setObjectName(QStringLiteral("fHardwareTab"));
        gridLayout_2 = new QGridLayout(fHardwareTab);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        fHardwareWidget = new QWidget(fHardwareTab);
        fHardwareWidget->setObjectName(QStringLiteral("fHardwareWidget"));

        gridLayout_2->addWidget(fHardwareWidget, 0, 0, 1, 1);

        ftabWidget->addTab(fHardwareTab, QString());

        gridLayout_4->addWidget(ftabWidget, 0, 0, 1, 1);


        retranslateUi(ControlPanel);

        ftabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(ControlPanel);
    } // setupUi

    void retranslateUi(QWidget *ControlPanel)
    {
        ControlPanel->setWindowTitle(QApplication::translate("ControlPanel", "Form", 0));
        fTestBox->setTitle(QApplication::translate("ControlPanel", "Tests", 0));
        fStartAllButton->setText(QApplication::translate("ControlPanel", "Start All", 0));
        fStartButton->setText(QApplication::translate("ControlPanel", "start", 0));
        fTestButton->setText(QApplication::translate("ControlPanel", "testbutton", 0));
        fTestControlLabel->setText(QApplication::translate("ControlPanel", "Test Control", 0));
        fPh2Path->setText(QApplication::translate("ControlPanel", "/home/readout/Alex/Ph2_ACF/", 0));
        fPh2_Label->setText(QApplication::translate("ControlPanel", "Ph2_ACF path", 0));
        ftabWidget->setTabText(ftabWidget->indexOf(fControlTab), QApplication::translate("ControlPanel", "Test Control", 0));
        ftabWidget->setTabText(ftabWidget->indexOf(fHardwareTab), QApplication::translate("ControlPanel", "Hardware", 0));
    } // retranslateUi

};

namespace Ui {
    class ControlPanel: public Ui_ControlPanel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONTROLPANEL_H
