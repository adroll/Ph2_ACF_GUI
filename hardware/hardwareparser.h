#ifndef HARDWAREPARSER_H
#define HARDWAREPARSER_H

#include <QWidget>
#include <QFile>
#include <QDebug>
#include <QFileDialog>
#include <QRadioButton>
#include <QLineEdit>
#include <QMessageBox>


#include "dommodel.h"
#include "domitem.h"
#include "parser/pugixml.hpp"



struct xmlFile
{
    QString name;
    QDomDocument *doc;
    QString path;
    bool changed;
};




namespace Ui {
class HardwareParser;
}

class HardwareParser : public QWidget
{
    Q_OBJECT

public:
    explicit HardwareParser(QWidget *parent = 0);
    ~HardwareParser();

private slots:
    bool refreshModel();

    QDomDocument *loadXML(QString cPath ="");

    void on_fLoadFile_clicked();

    void on_fSaveFile_clicked();


    //XML modifications
    void on_fAppendChild_clicked();

    void on_fRemoveLine_clicked();

    void on_fDublicateLine_clicked();

    void on_fSave_clicked();


    void showXMLfile(bool cChecked);

    void appendChildren(QDomDocument *cDomDocument, QDomNode &node, pugi::xml_node pNode);


public slots:
    void initialiseXMLfiles(QDomNodeList &cTestNodes);
    void initialiseRadioButtons();
    void updateCache();





signals:
    void status(QString cStatus);



public:
    QString fPh2ACFpath;

    QLineEdit *fPh2Path;

private:
    Ui::HardwareParser *ui;


    QDomNodeList fTestNodes;

    QString xmlPath;
    DomModel *fDomModel;
    QDomDocument fDomDocument;

    QString fXMLpath;

    QHash<QRadioButton*,xmlFile*> fRadioButtons;
    QList<xmlFile*> fCache;

    xmlFile* fCurrentFile;

    int fCount;


};

#endif // HARDWAREPARSER_H
