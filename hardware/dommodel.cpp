#include "domitem.h"
#include "dommodel.h"

#include <QtXml>

DomModel::DomModel(QDomDocument document, QObject *parent)
    : QAbstractItemModel(parent), domDocument(document)
{
    rootItem = new DomItem(domDocument, 0);
}

DomModel::~DomModel()
{
    delete rootItem;
}

int DomModel::columnCount(const QModelIndex &/*parent*/) const
{
    return 3;
}

QVariant DomModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    DomItem *item = static_cast<DomItem*>(index.internalPointer());

    QDomNode node = item->node();
    QStringList attributes;
    QDomNamedNodeMap attributeMap = node.attributes();

    switch (index.column()) {
        case 0:
        {
            QString name = node.nodeName();
            if(name == "#text")
                return "";//node.parentNode().nodeName();
            else
                return name;

            //return node.nodeName();
        }
        case 1:
            for (int i = 0; i < attributeMap.count(); ++i) {

                QDomNode attribute = attributeMap.item(i);
                attributes << attribute.nodeName() + "=\""
                              +attribute.nodeValue() + "\"";
                //if(i<attributeMap.count()-1)
                //    attributes <<"\n";
            }
            return attributes.join(" ");
        case 2:
            return node.nodeValue().split("\n").join(" ");
        default:
            return QVariant();
    }
}

Qt::ItemFlags DomModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}

QVariant DomModel::headerData(int section, Qt::Orientation orientation,
                              int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        switch (section) {
            case 0:
                return tr("Name");
            case 1:
                return tr("Attributes");
            case 2:
                return tr("Value");
            default:
                return QVariant();
        }
    }

    return QVariant();
}

QModelIndex DomModel::index(int row, int column, const QModelIndex &parent)
            const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    DomItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<DomItem*>(parent.internalPointer());

    DomItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex DomModel::parent(const QModelIndex &child) const
{
    if (!child.isValid())
        return QModelIndex();

    DomItem *childItem = static_cast<DomItem*>(child.internalPointer());
    DomItem *parentItem = childItem->parent();

    if (!parentItem || parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int DomModel::rowCount(const QModelIndex &parent) const
{
    if (parent.column() > 0)
        return 0;

    DomItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<DomItem*>(parent.internalPointer());

    return parentItem->node().childNodes().count();
}












//end simple dom model
bool DomModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role != Qt::EditRole)
        return false;
    DomItem *item = getItem(index);

    switch (index.column()) {
        case 0:
        {
            item->node().toElement().setTagName(value.toString());
            return true;
        }
        case 1:
        {
            return setAttributes(value, item);

        }
        case 2:
        {
            item->node().setNodeValue(value.toString());
            return true;
        }
        default:
            return false;
    }


}




bool DomModel::setAttributes(QVariant value, DomItem* item)
{
    QDomNamedNodeMap attributes =  item->node().attributes();

    for(int x = 0; x < attributes.count(); x++)
    {
        attributes.removeNamedItem(attributes.item(x).nodeName());
        x--;
    }

    QString cString = value.toString();

    QStringList splitted = cString.split(" ");

    for(int i = 0; i < splitted.size(); i++)
    {
        QString cElement = splitted.at(i);

        QStringList cElementList = cElement.split("=");
        if(cElementList.size() != 2)
        {
            qDebug() << "Falsche Eingabe";
            return false;
        }

        QString val = cElementList[1].mid(1,cElementList[1].size()-2);

        item->node().toElement().setAttribute(cElementList[0],val);

    }

    return true;
}



DomItem* DomModel::getItem(const QModelIndex &index) const
{
    if(index.isValid())
    {
        DomItem *item = static_cast<DomItem*>(index.internalPointer());
        if(item)
            return item;

    }
    return rootItem;
}



bool DomModel::insertChild(const QModelIndex &index)
{
    DomItem* parent = getItem(index);

    QDomNode childNode = domDocument.createElement("CHILD");
    QDomNode childValue = domDocument.createTextNode("CHILD-VALUE");
    childNode.appendChild(childValue);
    parent->node().appendChild(childNode);
    emit refreshModel();
    return true;
}

bool DomModel::deleteRow(const QModelIndex &index)
{

    DomItem *parent = getItem(index.parent());
    DomItem *child = getItem(index);

    parent->node().removeChild(child->node());
    emit refreshModel();
    return true;

}

bool DomModel::duplicateRow(const QModelIndex &index)
{

    DomItem *parent = getItem(index.parent());
    DomItem *child = getItem(index);
    QDomNode childNode = child->node().cloneNode();


    parent->node().insertAfter(childNode,child->node());
    emit refreshModel();
    return true;

}












