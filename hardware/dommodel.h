
#ifndef DOMMODEL_H
#define DOMMODEL_H

#include <QAbstractItemModel>
#include <QDomDocument>
#include <QModelIndex>
#include "domitem.h"

class DomItem;

class DomModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit DomModel(QDomDocument document, QObject *parent = 0);
    ~DomModel();

    QVariant data(const QModelIndex &index, int role) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &child) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;




    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    bool setAttributes(QVariant value, DomItem *item);

    bool insertChild(const QModelIndex &index);

    bool deleteRow(const QModelIndex &index);

    bool duplicateRow(const QModelIndex &index);

private:
    DomItem* getItem(const QModelIndex &index) const;



signals:
    void refreshModel();

private:
    QDomDocument domDocument;
    DomItem *rootItem;

};

#endif // DOMMODEL_H
