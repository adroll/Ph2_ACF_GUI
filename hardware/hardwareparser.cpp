#include "hardwareparser.h"
#include "ui_hardwareparser.h"

HardwareParser::HardwareParser(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HardwareParser)
{
    ui->setupUi(this);

    fDomModel = new DomModel(QDomDocument(), this);
    ui->fView->setModel(fDomModel);

    fCurrentFile = new xmlFile;
    fCurrentFile->changed = false;
    fCurrentFile->name = "";
    fCurrentFile->path = "";
    fCurrentFile->doc = new QDomDocument();
    fCount = 0;


}

HardwareParser::~HardwareParser()
{
    delete ui;
}



QDomDocument* HardwareParser::loadXML(QString cPath)
{  
    if(cPath =="")
    {
        cPath = QFileDialog::getOpenFileName(this, tr("Open File"),
            xmlPath, tr("XML files (*.xml);;HTML files (*.html);;"
                        "SVG files (*.svg);;User Interface files (*.ui)"));
    }
    else
    {
        if(!cPath.startsWith("/"))
        {
            cPath = fPh2Path->text() + cPath;
        }
    }
    emit status(cPath);

    QDomDocument *cDomDocument = new QDomDocument();

    fXMLpath = cPath;
    pugi::xml_document pDoc;

    if(pDoc.load_file(cPath.toStdString().c_str()))//,pugi::parse_declaration))
    {
        cDomDocument->appendChild(cDomDocument->createProcessingInstruction("xml", "version='1.0' encoding='UTF-8'"));
        for(pugi::xml_node child = pDoc.first_child(); child; child= child.next_sibling())
        {
            QDomNode node = cDomDocument->createElement(child.name());
            cDomDocument->appendChild(node);
            appendChildren(cDomDocument, node,child);
        }
    }
    else
    {
        QMessageBox::critical(this, tr("ERROR"), tr( pDoc.load_file(cPath.toStdString().c_str()).description()), QMessageBox::Ok);
        emit status(pDoc.load_file(cPath.toStdString().c_str()).description());
    }

    //fDomDocument = *cDomDocument;
    return cDomDocument;

}


void HardwareParser::appendChildren(QDomDocument* cDomDocument, QDomNode &node, pugi::xml_node pNode)
{
    if(pNode.child_value() !="")
    {
        QDomNode value = cDomDocument->createTextNode(pNode.child_value());
        node.appendChild(value);
    }
    for(pugi::xml_attribute_iterator it = pNode.attributes_begin(); it != pNode.attributes_end(); it++)
    {
        node.toElement().setAttribute(it->name(), it->value());
    }
    for(pugi::xml_node child = pNode.first_child(); child; child= child.next_sibling())
    {
        QDomNode childNode = cDomDocument->createElement(child.name());
        node.appendChild(childNode);
        appendChildren(cDomDocument, childNode,child);
    }
}



void HardwareParser::initialiseRadioButtons()
{
    QList<QRadioButton*> cKeyList = fRadioButtons.keys();

    for(QRadioButton* cButton : cKeyList)
    {
        delete cButton;
    }

    fRadioButtons.clear();

    delete ui->fXMLfiles->layout();

    QLayout* cLayout = new QGridLayout(ui->fXMLfiles);
    ui->fXMLfiles->setLayout(cLayout);
    for(int cNum = 0; cNum < fCache.size(); cNum++)
    {
        QRadioButton *cButton = new QRadioButton(ui->fXMLfiles);

        QString tmpName = fCache.at(cNum)->path;
        if(tmpName.size()>20)
        {
            tmpName = "..." + tmpName.mid(tmpName.size()-18,18);
        }


        cButton->setText(tmpName);
        cLayout->addWidget(cButton);
        fRadioButtons.insert(cButton,fCache[cNum]);
        connect(cButton,SIGNAL(toggled(bool)),this,SLOT(showXMLfile(bool)));
    }
    cLayout->update();
}



void HardwareParser::initialiseXMLfiles(QDomNodeList &cTestNodes)
{
    fTestNodes =cTestNodes;
}



void HardwareParser::showXMLfile(bool cChecked)
{
    for(QHash<QRadioButton*,xmlFile*>::iterator it = fRadioButtons.begin(); it != fRadioButtons.end(); it++)
    {
        if(it.key()->isChecked())
        {
            fCurrentFile = it.value();
            //fDomDocument = *(it.value()->doc);
            refreshModel();
        }
    }
}



bool HardwareParser::refreshModel()
{

    DomModel *newModel = new DomModel(*fCurrentFile->doc, this);

    ui->fView->setModel(newModel);
    delete fDomModel;
    fDomModel = newModel;
    connect(fDomModel, SIGNAL(refreshModel()),this,SLOT(refreshModel()));
    ui->fView->expandAll();

    ui->fView->resizeColumnToContents(0);
    ui->fView->setAlternatingRowColors(true);
    return true;

}


void HardwareParser::on_fLoadFile_clicked()
{
    fCount++;
    //fDomDocument = *loadXML();

    xmlFile* cFile = new xmlFile();
    cFile->doc = loadXML();
    cFile->name = fXMLpath;    
    cFile->path = fXMLpath;
    cFile->changed = false;
    bool test=false;
    for(int cNum = 0; cNum < fCache.size(); cNum++)
    {
        if(fCache.at(cNum)->path==cFile->path)
        {
            test = true;
            QMessageBox::warning(this, tr("Warning"), tr( "File already opened"), QMessageBox::Ok);
            fRadioButtons.key(fCache[cNum])->setChecked(true);
        }

    }
    if(!test)
    {
        fCache.append(cFile);
        fCurrentFile = cFile;
        initialiseRadioButtons();
    }

    refreshModel();
}



void HardwareParser::on_fAppendChild_clicked()
{
    QModelIndex index = ui->fView->currentIndex();
    if(!index.isValid())
        emit status("Please mark Node");
    else
    {
        fDomModel->insertChild(index);
        fCurrentFile->changed = true;
    }
}

void HardwareParser::on_fRemoveLine_clicked()
{
    QModelIndex index = ui->fView->currentIndex();
    if(!index.isValid())
        emit status("Please mark Node");
    else
    {
        fDomModel->deleteRow(index);
        fCurrentFile->changed = true;
    }

}

void HardwareParser::on_fDublicateLine_clicked()
{
    QModelIndex index = ui->fView->currentIndex();
    if(!index.isValid())
        emit status("Please mark Node");
    else
    {
        fDomModel->duplicateRow(index);
        fCurrentFile->changed = true;
    }
}


void HardwareParser::updateCache()
{
    //TODO
    //fXMLcache.clear();
    if(!fCache.size() == 0)
    {
        for(int cNum = 0; cNum < fCache.size(); cNum++)
        {
            if(fCache.at(cNum)->changed)
            {
                QString message = "\"" + fCache.at(cNum)->path + "\" was changed";
                int test = QMessageBox::critical(this, tr("ERROR"), tr( message.toStdString().c_str() ), QMessageBox::Save|QMessageBox::Discard);

                //saved pressed
                if(test == 0x00000800)
                {
                    fCurrentFile = fCache.at(cNum);
                    on_fSave_clicked();
                }
            }
        }
    }

    fCache.clear();


    QStringList cPathList;

    for(int cNum = 0; cNum < fTestNodes.size(); cNum++)
    {
        QDomNode TestNode = fTestNodes.at(cNum).namedItem("Options");
        QDomNode pathNode = TestNode.namedItem("XMLfile");

        if(!pathNode.isNull())
        {
            if(!cPathList.contains(pathNode.attributes().namedItem("value").nodeValue()))
            {
                //Cache only contains every hardware file one time! Not as many entries as Tests!
                xmlFile* cxmlFile = new xmlFile();
                cxmlFile->changed = false;
                cxmlFile->name = TestNode.parentNode().nodeName();
                cxmlFile->path = pathNode.attributes().namedItem("value").nodeValue();
                cxmlFile->doc = loadXML(pathNode.attributes().namedItem("value").nodeValue());
                fCache.append(cxmlFile);
                cPathList.append(cxmlFile->path);
            }

        }
    }
    initialiseRadioButtons();
    refreshModel();
}






void HardwareParser::on_fSaveFile_clicked()
{
    emit status("SaveAs");

    QString filePath = QFileDialog::getSaveFileName(this, tr("Save File"),
            xmlPath, tr("XML files (*.xml);;HTML files (*.html);;"
                        "SVG files (*.svg);;User Interface files (*.ui)"));




    QFile file( filePath );
    if( !file.open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
        emit status("No Valid file");
    }
    QTextStream stream( &file );
    stream << fCurrentFile->doc->toString();
    file.close();

}

void HardwareParser::on_fSave_clicked()
{
    QFile *file;
    if(fCurrentFile->path.startsWith("/"))
        file = new QFile(fCurrentFile->path);
    else
        file = new QFile(fPh2Path->text() + fCurrentFile->path);

    if( !file->open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
        QMessageBox::critical(this, tr("ERROR"), tr("Invalid path" ), QMessageBox::Ok);
        emit status("Invalid file");
    }
    QTextStream stream( file );
    stream << fCurrentFile->doc->toString();
    file->close();

    delete file;


}
