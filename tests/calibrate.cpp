#include "calibrate.h"


calibrate::calibrate(QObject *parent) : QObject(parent)
{
}





QString calibrate::GetCommand()
{
    //return "Ich bin ein calibrate";

    QString cCommand = "calibrate ";

    if(fBatch)
        cCommand += " -b";
    if(fNoise)
        cCommand += " -n";
    if(fSkip)
        cCommand += " -s";
    if(fOutput)
        cCommand += (" -o " + fOutputDirectory);

    cCommand += " -f " + fHWfile;

    //return "calibrate -b -f ";
    return cCommand;
}


