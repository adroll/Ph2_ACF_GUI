#include "testtab.h"
#include "ui_testtab.h"

TestTab::TestTab(QDomNode &cOptionNode, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TestTab)
{
    ui->setupUi(this);

    fLayout = new QGridLayout(parent);
    fWidget = parent;
    fNode = cOptionNode;

    makeTab();

}


TestTab::~TestTab()
{
    delete ui;
}



void TestTab::addOptionElement(QDomNode &node, int i)
{
    Option *cOption = new Option();
    cOption->name=node.nodeName();
    cOption->node=node;
    cOption->cBox = new QCheckBox(node.nodeName(), fWidget);
    cOption->cLine = NULL;

    QDomNamedNodeMap map = node.attributes();
    QDomNode command = map.namedItem("command");
    QDomNode value = map.namedItem("value");
    QDomNode state = map.namedItem("state");

    if(command!=QDomNode())
    {
        fCheckBoxList[command.nodeValue()] = cOption->cBox;
        fLayout->addWidget(cOption->cBox,i,0);
    }
    if(value!=QDomNode())
    {
        cOption->cLine = new QLineEdit(value.nodeValue(),fWidget);


        connect(cOption->cLine,SIGNAL(editingFinished()),cOption, SLOT(editLine()));
        connect(cOption->cLine,SIGNAL(editingFinished()),this,SIGNAL(reloadXMLFiles()));

        fLineEditList[command.nodeValue()] = cOption->cLine;
        fLayout->addWidget(cOption->cLine, i,1);
    }

    if(state !=QDomNode())
    {
        int cTmp = state.nodeValue().toInt();
        cOption->cBox->setChecked(cTmp);
    }



    fOptionList.append(cOption);
}



void TestTab::makeTab()
{


    QDomNodeList cOptionlist = fNode.childNodes();

    fOptionList.clear();


    for(int i = 0; i < cOptionlist.size();i++)
    {
        if(cOptionlist.at(i).nodeName()=="Options")
        {
            QDomNode cOptionNode = cOptionlist.at(i);
            for(int j = 0; j< cOptionNode.childNodes().size(); j++ )
            {
                QDomNode cNode = cOptionNode.childNodes().at(j);
                addOptionElement(cNode,j);
            }
        }
    }
}




QString TestTab::GetCommand()
{
    QString cCommand ="";
    QString cOptions = " ";
    QDomNodeList cNodeList = fNode.childNodes();

    for(int i = 0; i < cNodeList.size();i++)
    {

        if(fNode.childNodes().at(i).nodeName() == "Command")
        {
            cCommand += fNode.childNodes().at(i).childNodes().at(0).nodeValue();
        }
    }

    QHashIterator<QString, QCheckBox*> it(fCheckBoxList);

    while(it.hasNext())
    {
        it.next();
        if(it.value()->isChecked())
        {
            cOptions += " "+ it.key();
            if(fLineEditList.contains(it.key()))
            {
                cOptions += " " + fLineEditList[it.key()]->text();
            }
        }

    }


    return cCommand  + cOptions;


}


void TestTab::lineEdited(QString cNewText)
{
    for(Option* cOption : fOptionList)
    {
        if(cOption->cLine != NULL)
        {
            if(cOption->cLine->text() == cNewText)
            cOption->cLine->setText(cNewText + " BLA " );
        }
    }

}



QString TestTab::Test()
{
    QString Value;
    for(Option* cOption:fOptionList)
    {
        if(cOption->cLine !=NULL)
        {
            Value +="\n" + cOption->cLine->text();
            cOption->cLine->setText("BLA");
        }
    }
    return Value;
}


