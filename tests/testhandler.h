#ifndef TESTHANDLER_H
#define TESTHANDLER_H

#include <QObject>
#include <QObject>
#include <QThread>
#include <QDebug>
#include <QProcess>
#include <QFile>




class TestHandler : public QObject
{
    Q_OBJECT
public:
    explicit TestHandler(QObject *parent = 0);


private:
    QStringList fList;
public:
    QString fPh2Path;


signals:

    void print(QString cString);

    void finished();

public slots:

    void BuildList(QList<QString> cCommands);

    int run_script();
};

#endif // TESTHANDLER_H
