#ifndef TESTTAB_H
#define TESTTAB_H

#include <QWidget>
#include <QDomNode>
#include <QGridLayout>
#include <QCheckBox>
#include <QLine>
#include <QDebug>
#include <QLineEdit>
#include <QDomNodeList>


struct Option : public QObject
{

    Q_OBJECT
public:
    QString name;
    QDomNode node;
    QCheckBox* cBox;
    QLineEdit* cLine;

public slots:
    void editLine()
    {

        //TODO



        node.attributes().namedItem("value").setNodeValue(cLine->text());
        //cLine->setText(node.attributes().namedItem("value").nodeValue() + " SDLKFJS");


        //cLine->setText(cLine->text());
    }


};



namespace Ui {
class TestTab;
}

class TestTab : public QWidget
{
    Q_OBJECT

public:
    TestTab(QDomNode &OptionNode, QWidget *parent = 0);
    ~TestTab();

private slots:
    void makeTab();

    void addOptionElement(QDomNode &node, int i = 0);

public slots:
    QString GetCommand();

    void lineEdited(QString cNewText);

    QString Test();

signals:
    void reloadXMLFiles();

private:
    Ui::TestTab *ui;

public:
    QList<Option*> fOptionList;



    QHash<QString,QCheckBox*> fCheckBoxList;
    QHash<QString, QLineEdit*> fLineEditList;


    QWidget *fWidget;
    QGridLayout *fLayout;
    QDomNode fNode;

};

#endif // TESTTAB_H
