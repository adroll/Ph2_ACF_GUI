#ifndef CALIBRATE_H
#define CALIBRATE_H

#include <QObject>
#include <QWidget>
//#include <mainwindow.h>


#include "test.h"

class calibrate : public QObject , public Test
{
    Q_OBJECT
public:
    explicit calibrate(QObject *parent = 0);



    void setRun(bool pRun);

    QString GetCommand();
    //void GetSettings(QWidget *cSettingsTab);


    int fTargetVCth;

    bool fNoise;

    bool fSkip;

    bool fOutput;

    QString fOutputDirectory;

    bool fFile;

    QString fHWfile;

    bool fBatch;



signals:

public slots:
};

#endif // CALIBRATE_H
