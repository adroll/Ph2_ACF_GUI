#include "testhandler.h"

TestHandler::TestHandler(QObject *parent) : QObject(parent)
{

}




void TestHandler::BuildList(QList<QString> cCommands)
{
    fList.clear();

    QString command = "";
    for(int cNum = 0; cNum < cCommands.size(); cNum++)
    {
        command += "\n " +cCommands.at(cNum);
    }

    //command = "datatest -f "+fXmlFile;


    //fList << "/home/readout/Alex/Ph2_ACF_GUI/settings/script.sh"<<fPh2Path << "datatest -f settings/D19CDescription.xml";
    fList << "/home/readout/Alex/Ph2_ACF_GUI/settings/script.sh"<<fPh2Path << command;

    QString mystr = fList.join("");
    emit print(mystr);
}




int TestHandler::run_script()
{
   QProcess script;

   script.start("/usr/bin/bash", fList);
   QString cTmp;
   while(!script.waitForFinished(50))
   {
        cTmp = script.readAllStandardOutput();
        if(cTmp != "")
        emit print(cTmp);
    }
    cTmp = script.readAllStandardOutput();
    emit print(cTmp);

    emit print("Done");

    emit finished();
    return(1);

}
