#ifndef DATATEST_H
#define DATATEST_H

#include <QObject>

#include "test.h"


class datatest : public QObject , public Test
{
    Q_OBJECT
public:
    datatest(QObject *parent = 0);

    QString GetCommand();
private:
    //Options:
    int fVCth;
    int fEvents;
    bool fSave;
    bool fDaq;
    bool fRead;
    bool fIgnore;
    QString fFile;

};

#endif // DATATEST_H
