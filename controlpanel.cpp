#include "controlpanel.h"
#include "ui_controlpanel.h"

ControlPanel::ControlPanel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ControlPanel)
{
    ui->setupUi(this);

    fTestThread = new QThread();

    addTests();

    fHardwareParser = new HardwareParser(ui->fHardwareWidget);
    connect(fHardwareParser,SIGNAL(status(QString)),this, SIGNAL(status(QString)));

    fHardwareParser->initialiseXMLfiles(fTestNodes);
    fHardwareParser->fPh2Path = ui->fPh2Path;
    fHardwareParser->updateCache();






}



ControlPanel::~ControlPanel()
{
    delete ui;
}


void ControlPanel::updateHardwareParser()
{

    fHardwareParser->updateCache();



}




void ControlPanel::readTestXML()
{

    QString cPath = "../tests/tests.xml";
    QFile cFile(cPath);

    if(cFile.open(QIODevice::ReadOnly))
    {       
        fDomDoc = new QDomDocument();
        if(!fDomDoc->setContent(&cFile))
            ui->fConsole->append("XML not valid");
    }
    else
        ui->fConsole->append("ERROR XML not found");


    fTestNodes =fDomDoc->childNodes().at(1).childNodes();


}



void ControlPanel::addTests()
{
    //Read the XML file with the tests
    fTestTabs.clear();
    readTestXML();


    //add the Tests to The Control Panel
    for(int cTestNum = 0; cTestNum < fTestNodes.count(); cTestNum++)
    {
        QCheckBox *cBox = new QCheckBox(fTestNodes.at(cTestNum).nodeName());
        ui->fTestBoxLayout->addWidget(cBox);
        fCheckBoxList.append(cBox);
    }


    ui->fTestBoxLayout->update();

    //add new Tabs

    for(int cTestNum = 0; cTestNum < fTestNodes.count(); cTestNum++)
    {
        this->makeTestTab(fTestNodes.at(cTestNum));
    }


}


void ControlPanel::makeTestTab(QDomNode node)
{

    QWidget *cWidget = new QWidget();

    TestTab *cTab = new TestTab(node,cWidget);

    //connect(cTab,SIGNAL(reloadXMLFiles()),fHardwareParser,SLOT(updateCache()));
    connect(cTab,SIGNAL(reloadXMLFiles()),this,SLOT(updateHardwareParser()));


    fTestTabs.append(cTab);


    ui->ftabWidget->addTab(cWidget,node.nodeName());
}


void ControlPanel::on_fStartButton_clicked()
{
    emit status("HIER TEST");

    if(fTestThread->isRunning())
    {
        appendLine("Thread is still running");
    }

    else
    {
        ui->fConsole->clear();

        fTestHandler = new TestHandler();
        fTestHandler->fPh2Path = ui->fPh2Path->text();


        QList<QString> cCommandList;
        cCommandList.clear();
        for(int cTest = 0; cTest < fCheckBoxList.size(); cTest++)
        {
            if(fCheckBoxList.value(cTest)->isChecked())
            {
                ui->fConsole->append(fTestTabs[cTest]->GetCommand());


                cCommandList.append(fTestTabs[cTest]->GetCommand());
            }
        }


        fTestHandler->moveToThread(fTestThread);
        connect(fTestThread,SIGNAL(started()),fTestHandler,SLOT(run_script()));
        connect(fTestHandler,SIGNAL(finished()),fTestThread,SLOT(quit()));
        connect(fTestHandler, SIGNAL (finished()), fTestHandler,SLOT(deleteLater()));
        connect(fTestHandler,SIGNAL(print(QString)),this,SLOT(appendLine(QString)));

        ui->fConsole->append("Start tests:" );

        fTestHandler->BuildList(cCommandList);

        fTestThread->start();

    }





}

void ControlPanel::on_fStartAllButton_clicked()
{
    for(int i = 0; i < fCheckBoxList.size();i++)
    {
        fCheckBoxList.value(i)->setChecked(true);
    }
    this->on_fStartButton_clicked();
}



void ControlPanel::appendLine(QString cString)
{
    ui->fConsole->append(cString);
}





void ControlPanel::resizeEvent(QResizeEvent *e)
{
    fHardwareParser->resize(ui->fHardwareWidget->width(),ui->fHardwareWidget->height());

}



void ControlPanel::on_fTestButton_clicked()
{
    ui->fTextBrowser->clear();


    ui->fTextBrowser->append("Update cache");

    fHardwareParser->updateCache();



    for(int cTestNum = 0; cTestNum < fTestNodes.count(); cTestNum++)
    {

        QDomNode TestNode = fTestNodes.at(cTestNum);

        QDomNode OptionsNode = TestNode.namedItem("Options");

        QDomNode myOption = OptionsNode.namedItem("XMLfile");

        ui->fTextBrowser->append(myOption.attributes().namedItem("value").nodeValue());

        //ui->fTextBrowser->append(TestNode.nodeName());
    }




}
